/*// Utility class */
var utils = (function(){
    
    getQuerystring = function (key, default_)
    {
      if (default_==null) default_="";
      key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
      var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
      var qs = regex.exec(window.location.href);

      return qs == null ? default_ : qs[1];
    }

    // Returns the left / top / width / height of $(item)
    getItemLayoutProp = function( $item ) {
           
        var scrollT = $(window).scrollTop(),
          scrollL = $(window).scrollLeft(),
          itemOffset = $item.offset();

        return {
          left : itemOffset.left - scrollL,
          top : itemOffset.top - scrollT,
          width : $item.outerWidth(),
          height : $item.outerHeight()
        };
    }

    // Device detection
    getUserAgent = function(){
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        return isMobile;
    }
    
    return {
        getQuerystring : getQuerystring,
        getItemLayoutProp :getItemLayoutProp,
        getUserAgent : getUserAgent
    }
    
})()

var mapUtils = (function(){

    var heatmap =null;
    console.log('heatmap',heatmap);

    encodeNumber =  function (num) {

        var encodeString = "";

        while (num >= 0x20) {
            encodeString += (String.fromCharCode((0x20 | (num & 0x1f)) + 63));
            num >>= 5;
        }

        encodeString += (String.fromCharCode(num + 63));
        return encodeString;

    };

    encodeSignedNumber = function (num) {

        var sgn_num = num << 1;

        if (num < 0) {
            sgn_num = ~(sgn_num);
        }

        return (this.encodeNumber(sgn_num));

    };

    encodePoints = function (ar) {

        var i = 0;
        var plat = 0;
        var plng = 0;
        var encoded_points = "";

        // console.log('@encodePoints');
        // console.dir(ar);

        for (i = 0; i < ar.length; ++i) {
            var point = ar[i];
            var lat = point.latitude;
            var lng = point.longitude;

            var late5 = Math.floor(lat * 1e5);
            var lnge5 = Math.floor(lng * 1e5);

            dlat = late5 - plat;
            dlng = lnge5 - plng;

            plat = late5;
            plng = lnge5;

            encoded_points += this.encodeSignedNumber(dlat) + this.encodeSignedNumber(dlng);
        }

        return encoded_points;
    };

    decodePoints = function (encoded, reverse, engine) {

        var len = String(encoded).length;
        var index = 0;
        var ar = [];
        var lat = 0;
        var lng = 0;

        try {

            while (index < len) {

                var b;
                var shift = 0;
                var result = 0;
                do {
                    b = encoded.charCodeAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                }
                while (b >= 0x20);

                var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charCodeAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                }
                while (b >= 0x20);

                var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
                lng += dlng;


                switch (engine) {

                    case 'google':

                        if (reverse) {
                            ar.push(new google.maps.LatLng((lng * 1e-5), (lat * 1e-5)));
                        }
                        else {
                            ar.push(new google.maps.LatLng((lat * 1e-5), (lng * 1e-5)));
                        }

                        break; // | google

                    case 'bing7':

                        if (reverse) {
                            ar.push(new Microsoft.Maps.Location((lng * 1e-5), (lat * 1e-5)));
                        }
                        else {
                            ar.push(new Microsoft.Maps.Location((lat * 1e-5), (lng * 1e-5)));
                        }

                        break; // | bing7

                    case 'bing6':

                        if (reverse) {
                            ar.push(new VELatLong((lng * 1e-5), (lat * 1e-5)));
                        }
                        else {
                            ar.push(new VELatLong((lat * 1e-5), (lng * 1e-5)));
                        }

                        break; // | bing 6                       

                    default:

                        alert('Error: encodedecode not supported for this engine');
                        return;
                }
            }
        }

        catch (ex) {
            //error in encoding.
        }

        return ar;

    };

    addPolygonesLayer = function(layer, map){
      var i = layer.length;
      while (i--) {
        addPolygon(layer[i], map);
      };
    };

    addPolygon = function(map, pol, col, weight){
        // Define the LatLng coordinates for the polygon's path.

        
        // build the polygon.
        polygone = new google.maps.Polygon({
          paths: pol,
          strokeColor: col,
          strokeOpacity: 0.8,
          strokeWeight: 1,
          fillColor: col,
          fillOpacity: weight
        });

        polygone.setMap(map);

      };

    addHeatPoints = function(map, pts){

      var pointArray = new google.maps.MVCArray(pts);
      console.log('this.heatmap %o', heatmap);

      heatmap = new google.maps.visualization.HeatmapLayer({
          data: pointArray
        });
        heatmap.setMap(map);
        heatmap.setOptions({radius: 5, dissipating: true});
    }

    getMapStyle = function(pointArray){
      
    }

    onZoomChanged = function(e){

      if(heatmap !== null)
      heatmap.setOptions({radius:e});
    }

    getNewRadius = function(){

    }

    return {
        encodePoints : encodePoints,
        decodePoints : decodePoints,
        addPolygon : addPolygon,
        addHeatPoints :addHeatPoints,
        onZoomChanged : onZoomChanged
    };

})()

var mathUtils = (function(){

    function randomFromInterval(from,to)
    {
        return Math.floor(Math.random()*(to-from+1)+from);
    }

    return {
      randomFromInterval : randomFromInterval
    }


})()