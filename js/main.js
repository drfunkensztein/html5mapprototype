
$(document).ready(function(){

  new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );

  var dragPos = 0;

  window.onresize = function(){
    $('#road').css('clip', 'rect(0px, ' + utils.getItemLayoutProp($('.content')).width + 'px, ' + utils.getItemLayoutProp($('.content')).height + 'px, ' + dragPos + 'px)');
  }

  var roadMapOptions  = {
    title : "my road map",
    wrapper : "road",
    type : "road",
    scrollwheel: true,
    scaleControl: false,
    mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
    center: new google.maps.LatLng(-28.023500 , 153.378754),
    zoom: 11,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  }

  var satMapOptions  = {
    title : "my road map",
    wrapper : "sat",
    type : "sat",
    scrollwheel: true,
    scaleControl: false,
    mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
    center: new google.maps.LatLng(-28.023500 , 153.378754),
    zoom: 11,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  }

  var road = new Map(roadMapOptions);

  google.maps.event.addListener(road.map, 'idle', function ( ) {
    throttled();
    console.log(road.map.getZoom());
  });

  updatePoly = function(){

    var endpoint = "http://economy.local/sunshine-coast/geo/placemakerarea/?",
        nelat = road.map.getBounds().getNorthEast().lat(),
        nelon = road.map.getBounds().getNorthEast().lng(),
        swlat = road.map.getBounds().getSouthWest().lat(),
        swlon = road.map.getBounds().getSouthWest().lng(),     
        zoom  = road.map.getZoom(),
        layer = isInRange(road.map.getZoom(), 5, 8) ? "SA4" : isInRange(road.map.getZoom(), 9, 13) ? "SA2" : isInRange(road.map.getZoom(), 14, 19) ? "SA1" : "SA0"
 
    var url = endpoint + "&nelat=" + nelat + "&nelon=" + nelon + "&swlat=" + swlat + "&swlon=" + swlon + "&zoom=" + zoom + "&layer=" + layer;
    console.log('Ajax call \n url: %O  ', url);

    $.getJSON( url, function( layer ) {
        console.log('layer: %O', layer.layers[0].shapes);
        road.addLayer(1, layer.layers[0].shapes, "polyLayer");
    })
  }

  var throttled = _.throttle(updatePoly, 50);

  //var sat = new Map(satMapOptions);
  // road.bindTo('center', sat, 'center');
  // road.bindTo('zoom', sat, 'zoom');

  // jsonh Test
  $.getJSON( "js/shapesid6.js", function( layer ) {
    var shapes = jsonh.pack(layer);

    //console.log('Shapes: %o | shp: %o | unpacked: %o', JSON.stringify(shapes), shapes, jsonh.unpack(shapes));
  })


  $.getJSON( "js/shapesid6.js", function( layer ) {
    var heatpts = [],
        i       = layer.length;

    //road.addLayer(1, layer, "polyLayer");
    //console.log('from main road.mylayer:%o ', this.layer);

    while (i--) {
      weight = Math.random();
      heatpts.push({location : mapUtils.decodePoints(layer[i].centroid, true, "google")[0], weight:weight})
    };
    //road.addLayer(layer, "heatLayer");
  });
 
  $('#swipeDiv').udraggable({
    axis : "x",
    drag: function (e, ui) {
      var pos = ui.position;
      dragPos = pos.left;
      $('#road').css('clip', 'rect(0px, '+utils.getItemLayoutProp($('.content')).width+'px, '+utils.getItemLayoutProp($('.content')).height+'px, '+pos.left+'px)')
  }}); 
});
