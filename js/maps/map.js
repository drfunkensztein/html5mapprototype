// Map base class
var Map = Fiber.extend(function() {
    
    return {
        // The `init` method serves as the constructor.
        init: function(config) {
            
            // private var
            var el = document.getElementById(config.wrapper);

            // public var
            this.map            = new google.maps.Map(el, config);
            this.heatmap        = null;
            this.layer          = null;
            this.polsCollection = [];
            this.layers = {};

            // private methods
            private1 = function(id, layerData, layerType){
                console.log("private1 called from Map", id, layerData, layerType)
            };

            // Privileged methods 
            this.privileged1 = function(){
                //can call private function directly
            }
            this.privileged2 = function(){}
        },

        // Public methods 
        method1: function(){
            // can call privileged methods but not the private functions
        },

        addLayer : function(id, layerData, layerType){

            // this.layers["layer"+id] = new MapLayer(id, layerData, layerType);
            // console.log('mylayer: %O',this.layers["layer"+id]);
            // this.layers["layer"+id].method1();
            
            this.hideLayer();
            this.layer =  layerData;
            var i = this.layer.length;
            this.polsCollection = [];

            // _.each(this.layer, function(item){
            //     this.polsCollection.push(this.getPolygon(mapUtils.decodePoints(item.points, true, "google"), '#66CCCC', _.random(0.2, 0.8) ));
            // }, this)

            while (i--) {
                this.polsCollection.push(this.getPolygon(mapUtils.decodePoints(this.layer[i].points, true, "google"), '#66CCCC', Math.random()));
            };
            this.showLayer();
        },

        getPolygon :function(pol, col, weight){
            // build the polygon.
            return new google.maps.Polygon({
              paths: pol,
              strokeColor: col,
              strokeOpacity: 0.8,
              strokeWeight: 1,
              fillColor: col,
              fillOpacity: weight
            });
        },

        addHeatPoints : function(pts){
            var pointArray = new google.maps.MVCArray(pts);
            this.heatmap = new google.maps.visualization.HeatmapLayer({
              data: pointArray
            });
            this.heatmap.setMap(this.map);
            this.heatmap.setOptions({radius: 5, dissipating: true});
        },

        hideLayer :function(){
            if(this.polsCollection !== null){
                i = this.polsCollection.length;
                while (i--) {
                    this.polsCollection[i].setMap(null);
                };
            }
        },

        showLayer :function(){
            if(this.polsCollection !== null){
                i = this.polsCollection.length;
                while (i--) {
                    this.polsCollection[i].setMap(this.map);
                };
            }
        },

        getMapStyle : function(pointArray){
          
        },

        onZoomChanged : function(e){

          if(heatmap !== null)
          this.heatmap.setOptions({radius:e});
        },

        getNewRadius : function(){

        },

        bindTo : function(prop, mp){
            this.map.bindTo(prop, mp.map, prop);
        }


    } // end of return
}); //Fiber.extend