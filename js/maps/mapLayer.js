// Map base class
var MapLayer = Fiber.extend(function() {
    
    return {
        // The `init` method serves as the constructor.
        init: function(id, layerData, layerType) {
            
            this.layerId    = id;
            this.layerType  = layerType;
            this.layerData  = layerData;

            // private var
            // private methods
            private1 = function(id, layerData, layerType){
                console.log('laer: %o: ', this.layerId );
            };


            // Privileged methods 
            this.privileged1 = function(){
                //can call private function directly
                private1(id, layerData, layerType)

            }
            this.privileged2 = function(){}
        },

        // Public methods 
        method1: function(){
            // can call privileged methods but not the private functions
             console.log('method1: %o: ', this.layerId );
        },

        addLayer : function(layerData, layerType){
            this.layer =  layerData;
            var i = this.layer.length;
            while (i--) {
                this.polsCollection = []; // empty array
                this.polsCollection.push(this.getPolygon(mapUtils.decodePoints(this.layer[i].points, true, "google"), '#66CCCC', Math.random()));
            };
            this.showLayer();
        },

        getPolygon :function(pol, col, weight){
            // build the polygon.
            return new google.maps.Polygon({
              paths: pol,
              strokeColor: col,
              strokeOpacity: 0.8,
              strokeWeight: 1,
              fillColor: col,
              fillOpacity: weight
            });
        },

        addHeatPoints : function(pts){
            var pointArray = new google.maps.MVCArray(pts);
            this.heatmap = new google.maps.visualization.HeatmapLayer({
              data: pointArray
            });
            this.heatmap.setMap(this.map);
            this.heatmap.setOptions({radius: 5, dissipating: true});
        },

        hideLayer :function(){
            if(this.polsCollection !== null){
                i = this.polsCollection.length;
                while (i--) {
                    this.polsCollection[i].setMap(null);
                };
            }
        },

        showLayer :function(){
            if(this.polsCollection !== null){
                i = this.polsCollection.length;
                while (i--) {
                    this.polsCollection[i].setMap(this.map);
                };
            }
        },

        getMapStyle : function(pointArray){
          
        },

        onZoomChanged : function(e){

          if(heatmap !== null)
          this.heatmap.setOptions({radius:e});
        },

        getNewRadius : function(){

        },

        bindTo : function(prop, mp){
            this.map.bindTo(prop, mp.map, prop);
        }


    } // end of return
}); //Fiber.extend